package tests;

import base.BasePageUtil;
import org.junit.Before;
import org.junit.Test;
import pages.SearchPage;

public class SearchTest extends BasePageUtil {

    SearchPage searchPage;

    @Before
    public void init() throws Exception{
        searchPage = new SearchPage(driver);
    }

    @Test
    public void successfulSearch(){
        searchPage.search(searchTerm);
    }

    @Test
    public void noResultSearch(){
        searchPage.search(wrongSearchTerm);
    }


}
