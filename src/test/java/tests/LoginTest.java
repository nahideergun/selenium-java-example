package tests;

import base.BaseTest;
import org.junit.Before;
import org.junit.Test;
import pages.LoginPage;

public class LoginTest extends BaseTest {

    LoginPage loginPage;

    @Before
    public void init() throws Exception{
        loginPage = new LoginPage(driver);
    }
    @Test
    public  void successfulLoginTest(){
        loginPage.successfulLogin(username,password);
    }

    @Test
    public void unsuccessfulLoginTest(){
        loginPage.unsuccessfulLogin(wrongUsername,password);
    }
}
