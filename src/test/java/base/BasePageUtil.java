package base;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class BasePageUtil extends BaseTest {

    public WebElement clickObjectBy(WebElement element) {
        element.click();
        return element;
    }

    public WebElement setObjectBy(WebElement element, String text) {
        element.sendKeys(text);
        return element;
    }


    public boolean isElementExist(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static void appendTextToFile(String fileName,
                                        String text) {
        try {
            BufferedWriter out = new BufferedWriter(
                    new FileWriter(fileName, true));
            out.write(text + " \n");
            out.close();
        } catch (IOException e) {
            System.out.println("exception occoured" + e);
        }
    }

    public Date getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        return date;
    }
}
