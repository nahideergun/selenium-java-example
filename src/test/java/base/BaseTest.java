package base;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

public class BaseTest {

    protected static WebDriver driver;
    public static PropertyLoader propertyLoader;
    public String username;
    public String password;
    public String wrongUsername;
    public String searchTerm;
    public String wrongSearchTerm;


    @Before
    public void setUp() {

        Boolean isChromeDriver = true;
        String baseUrl = "https://www.n11.com/";
        DesiredCapabilities capabilities;
        propertyLoader = new PropertyLoader();

        username = propertyLoader.getProperty("username");
        password = propertyLoader.getProperty("password");
        wrongUsername = propertyLoader.getProperty("wrongUsername");
        searchTerm = propertyLoader.getProperty("searchTerm");
        wrongSearchTerm = propertyLoader.getProperty("wrongSearchTerm");

        if (isChromeDriver) {
            capabilities = DesiredCapabilities.chrome();
            ChromeOptions options = new ChromeOptions();
            options.addArguments("--start-fullscreen");
            options.addArguments("--disable-notifications");
            capabilities.setCapability(ChromeOptions.CAPABILITY, options);
            System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
            driver = new ChromeDriver(capabilities);
        } else {
            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
            driver = new FirefoxDriver();
        }


        driver.get(baseUrl);
    }

    @After
    public void tearDown() {
        if (driver != null) {
            driver.quit();

        }
    }

    public WebDriver getWebDriver() {
        return driver;
    }

    public void setWebDriver(WebDriver webDriver) {
        this.driver = webDriver;
    }
}
