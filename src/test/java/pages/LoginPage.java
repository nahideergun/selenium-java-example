package pages;

import base.BasePageUtil;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.List;

public class LoginPage extends BasePageUtil {
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }


    @FindBy(className = "btnSignIn")
    public WebElement SigninButton;

    @FindBy(id = "email")
    public WebElement emailLabel;

    @FindBy(id = "password")
    public WebElement passwordLabel;

    @FindBy(id = "loginButton")
    public WebElement loginButton;

    @FindBy(className = "user")
    public WebElement userNameField;

    @FindBy(className = "errorText")
    public List<WebElement> errorTexts;

    public void successfulLogin(String username, String password) {
        clickObjectBy(SigninButton);
        setObjectBy(emailLabel, username);
        setObjectBy(passwordLabel, password);
        clickObjectBy(loginButton);
        Assert.assertTrue("Failed to login.", isElementExist(userNameField));
    }

    public void unsuccessfulLogin(String username, String password) {
        clickObjectBy(SigninButton);
        setObjectBy(emailLabel, username);
        setObjectBy(passwordLabel, password);
        clickObjectBy(loginButton);
        String errorMessage = errorTexts.get(2).getText();
        appendTextToFile("logginError.txt", getCurrentDate() + " " +errorMessage);
        Assert.assertTrue("Logged in even though it should not be logged in.", !isElementExist(userNameField));

    }


}
