package pages;

import base.BasePageUtil;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchPage extends BasePageUtil {

    public SearchPage(WebDriver driver) {
        this.setWebDriver(driver);
        PageFactory.initElements(this.getWebDriver(), this);
    }

    @FindBy(id = "searchData")
    public WebElement searchInput;

    @FindBy(className = "iconSearch")
    public WebElement searchIcon;

    @FindBy(className = "resultText")
    public WebElement searchResultText;

    @FindBy(css = ".resultText strong")
    public WebElement searchResultCount;

    @FindBy(css = ".listView .column")
    public WebElement productCells;

    @FindBy(className = "notFoundContainer")
    public WebElement notFoundContainer;

    public void search(String searchTerm) {
        setObjectBy(searchInput, searchTerm);
        clickObjectBy(searchIcon);

        if (searchTerm.contains("no")) {
            Assert.assertTrue(("Products are listed even though there should be no result for term " + searchTerm), isElementExist(notFoundContainer));
            appendTextToFile("result.txt", "No result found for search term '" + searchTerm + "'");
        } else {
            Assert.assertTrue(("No result found for term " + searchTerm), isElementExist(searchResultText) && isElementExist(productCells));
            appendTextToFile("result.txt", searchResultCount.getText() + " result found for search term '" + searchTerm + "'");
        }
    }
}
